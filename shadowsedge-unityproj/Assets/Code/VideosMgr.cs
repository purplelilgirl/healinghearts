﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideosMgr : MonoBehaviour
{
    public float m_animTime = 0.5f;

    public GameObject m_popupBG;
    public VideoPlayerPopup m_videoPlayerPopup;

    public VideoButton m_videoButton;

    private void Start()
    {
        LoadVideos();

        // if from main scene, show video player automatically
        if (PlayerPrefs.GetInt("FromMainScene", 0) == 1)
        {  ShowVideoPlayer(PlayerPrefs.GetInt("MainSceneVideoIdx", 0), null, false);
        }   
        else
        {   m_popupBG.SetActive(false);
            m_videoPlayerPopup.gameObject.SetActive(false);
        }
    }

    // Show screenies
    void LoadVideos()
    {
        Debug.Log("LoadVideos");

        int videosCount = PlayerPrefs.GetInt("CurrVideoIdx", 0);

        if(videosCount == 0)
        {   m_videoButton.gameObject.SetActive(false);
        }

        for (int i = 0; i < videosCount; i++)
        {
            Debug.Log("LoadVideos " + i);

            VideoButton videoButton;

            if(i == 0)
            {   videoButton = m_videoButton;
            }
            else
            {   GameObject videoObj = Instantiate(m_videoButton.gameObject, m_videoButton.transform.parent) as GameObject;
                videoButton = videoObj.GetComponent<VideoButton>();
            }

            videoButton.m_videoIdx = i;
            videoButton.m_videosMgr = this;

            videoButton.GetComponent<Button>().onClick.AddListener(videoButton.ShowVideo);

            string title = PlayerPrefs.GetString(i + "VideoTitle", "Video " + (i + 1).ToString());
            string feelings = PlayerPrefs.GetString(i + "Feelings", "0happy");

            videoButton.m_title.text = title;
            videoButton.m_emoji.sprite = Resources.Load("Icons/Emotions/" + feelings, typeof(Sprite)) as Sprite;

            bool fileNotFound = false;

            try
            {
                Texture2D texture;

                byte[] bytes;
                bytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + "Screenshot" + i + ".png");
                texture = new Texture2D(1, 1);
                texture.LoadImage(bytes);

                videoButton.m_screenshot.texture = texture;

                fileNotFound = false;
            }
            catch (System.Exception e)
            {
                Debug.Log("Screenshot Not Found " + e);
                fileNotFound = true;
            }

            string url = PlayerPrefs.GetString(i + "VideoPath", "");

            if (url != "" && System.IO.File.Exists(url))
            {   fileNotFound = false;
            }   else
            {   Debug.Log("Video Not Found");
                fileNotFound = true;
            }

            if (fileNotFound)
            {
                Debug.Log("FileNotFound");
                //videoButton.m_screenshot.color = Color.black;
                videoButton.m_notFound.SetActive(true);
            }   else
            {   videoButton.m_notFound.SetActive(false);
            }
        }
    }

    public void ShowVideoPlayer(int currIdx, Texture screenshot=null, bool willAnim=true)
    {
        m_popupBG.SetActive(true);

        string url = PlayerPrefs.GetString(currIdx + "VideoPath", "");
        string title = PlayerPrefs.GetString(currIdx + "VideoTitle", "Video " + (currIdx + 1).ToString());
        string description = PlayerPrefs.GetString(currIdx + "VideoDescription", "");

        string feelings = PlayerPrefs.GetString(currIdx + "Feelings", "0happy");

        m_videoPlayerPopup.m_videoTitle.text = title;
        m_videoPlayerPopup.m_videoDescription.text = description;

        m_videoPlayerPopup.m_emoji.sprite = Resources.Load("Icons/Emotions/" + feelings, typeof(Sprite)) as Sprite;

        if (screenshot == null)
        {
            Texture2D texture;

            try
            {
                byte[] bytes;
                bytes = System.IO.File.ReadAllBytes(Application.persistentDataPath + "Screenshot" + currIdx + ".png");
                texture = new Texture2D(1, 1);
                texture.LoadImage(bytes);

                m_videoPlayerPopup.m_background.texture = texture;
            }
            catch (System.Exception e)
            {
                Debug.Log("Screenshot Not Found " + e);

                m_popupBG.SetActive(false);
                m_videoPlayerPopup.gameObject.SetActive(false);

                return;
            }           
        }
        else
        {   m_videoPlayerPopup.m_background.texture = screenshot;
        }

        if (url != "" && System.IO.File.Exists(url))
        {
            m_videoPlayerPopup.SetVideoPlayer(url);
        }   else
        {
            Debug.Log("Video Not Found");

            m_popupBG.SetActive(false);
            m_videoPlayerPopup.gameObject.SetActive(false);

            return;
        }

        if(willAnim)
        {   m_videoPlayerPopup.transform.localScale = Vector3.zero;
            LeanTween.scale(m_videoPlayerPopup.gameObject, Vector3.one, m_animTime);
        }

        m_videoPlayerPopup.gameObject.SetActive(true);
    }

    public void CloseVideoPlayer()
    {
        BGMusic.Instance.PlayPopupSfx();
        LeanTween.scale(m_videoPlayerPopup.gameObject, Vector3.zero, m_animTime).setOnComplete(HideVideoPlayerPopup);
    }
    
    void HideVideoPlayerPopup()
    {
        m_popupBG.SetActive(false);
        m_videoPlayerPopup.gameObject.SetActive(false);
    }

    public void LoadHomeScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        SceneManager.LoadScene("HomeScene");
    }
}
