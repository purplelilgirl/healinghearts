﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoButton : MonoBehaviour
{
    public Text m_title;
    public RawImage m_screenshot;
    public Image m_emoji;

    public GameObject m_notFound;

    [HideInInspector]
    public int m_videoIdx;

    [HideInInspector]
    public VideosMgr m_videosMgr;

    public void ShowVideo()
    {
        BGMusic.Instance.PlayPopupSfx();
        m_videosMgr.ShowVideoPlayer(m_videoIdx, m_screenshot.texture);
    }
}
