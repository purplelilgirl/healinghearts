﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PuzzleMgr : MonoBehaviour
{
    public WayOfWellbeing m_waysOfWellbeing;

    public GameObject m_puzzle;
    public Puzzle[] m_puzzles;
    private int m_completedPuzzle = 0;

    public Material m_normalMat;

    public float forceAmount = 500;
    public Camera targetCamera;

    Rigidbody selectedRigidbody;
    Vector3 originalScreenTargetPosition;
    Vector3 originalRigidbodyPos;
    float selectionDistance;

    public GameObject m_instruction;

    public GameObject m_popup;
    public GameObject m_completePopup;
    public Text m_completeTxt;

    public float m_animTime = 0.2f;

    private void Start()
    {
        for (int i = 0; i < m_puzzles.Length; i++)
        {   m_puzzles[i].GetComponent<Rigidbody>().isKinematic = true;
            m_puzzles[i].GetComponent<Rigidbody>().freezeRotation = true;
            m_puzzles[i].GetComponent<SpriteRenderer>().sortingOrder = 1;
        }

        m_puzzle.SetActive(true);
        m_instruction.SetActive(true);
        m_popup.SetActive(false);
        m_completePopup.SetActive(false);
    }

    void Update()
    {
        if (!targetCamera)
            return;


        if (Input.GetMouseButtonDown(0))
        {
            //Check if we are hovering over Rigidbody, if so, select it
            selectedRigidbody = GetRigidbodyFromMouseClick();

            if (selectedRigidbody)
            {
                selectedRigidbody.isKinematic = false;
                selectedRigidbody.GetComponent<BoxCollider>().enabled = false;
                selectedRigidbody.GetComponent<SpriteRenderer>().sortingOrder = 2;
            }
        }

        if (Input.GetMouseButtonUp(0) && selectedRigidbody)
        {
            //Release selected Rigidbody if there any
            selectedRigidbody.isKinematic = true;
            selectedRigidbody.GetComponent<BoxCollider>().enabled = true;
            selectedRigidbody.GetComponent<SpriteRenderer>().sortingOrder = 1;
            selectedRigidbody = null;
        }
    }

    void ShowComplete()
    {
        BGMusic.Instance.PlayPopupSfx();

        //m_completeTxt.text = "Congrats! You completed the puzzle and unlocked \"" + m_waysOfWellbeing.ToString() + "\" Tips.";
        PlayerPrefs.SetInt(m_waysOfWellbeing.ToString() + "UnlockedTips", 1);

        m_popup.SetActive(true);

        m_puzzle.SetActive(false);

        m_completePopup.transform.localScale = Vector3.zero;
        m_completePopup.SetActive(true);
        LeanTween.scale(m_completePopup, Vector3.one, m_animTime);
    }

    void FixedUpdate()
    {
        if (selectedRigidbody)
        {
            Vector3 mousePositionOffset = targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, selectionDistance)) - originalScreenTargetPosition;
            selectedRigidbody.velocity = (originalRigidbodyPos + mousePositionOffset - selectedRigidbody.transform.position) * forceAmount * Time.deltaTime;

            Vector3 pos = new Vector3(selectedRigidbody.GetComponent<Puzzle>().m_origPos.x, selectedRigidbody.GetComponent<Puzzle>().m_origPos.y, selectedRigidbody.transform.position.z);

            if (Vector3.Distance(selectedRigidbody.transform.position, pos) < 0.1f)
            {
                selectedRigidbody.transform.position = selectedRigidbody.GetComponent<Puzzle>().m_origPos;
                selectedRigidbody.GetComponent<SpriteRenderer>().sortingOrder = 1;
                selectedRigidbody.GetComponent<SpriteRenderer>().material = m_normalMat;
                selectedRigidbody.isKinematic = true;
                selectedRigidbody = null;

                m_completedPuzzle++;

                BGMusic.Instance.PlayBtnSfx();

                // finished puzzle
                if (m_completedPuzzle >= m_puzzles.Length)
                {
                    m_instruction.SetActive(false);
                    Invoke("ShowComplete", 1);
                }
            }
        }
    }

    Rigidbody GetRigidbodyFromMouseClick()
    {
        RaycastHit hitInfo = new RaycastHit();
        Ray ray = targetCamera.ScreenPointToRay(Input.mousePosition);
        bool hit = Physics.Raycast(ray, out hitInfo);
        if (hit)
        {
            if (hitInfo.collider.gameObject.GetComponent<Rigidbody>())
            {
                selectionDistance = Vector3.Distance(ray.origin, hitInfo.point);
                originalScreenTargetPosition = targetCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, selectionDistance));
                originalRigidbodyPos = hitInfo.collider.transform.position;
                return hitInfo.collider.gameObject.GetComponent<Rigidbody>();
            }
        }

        return null;
    }

    public void LoadHomeScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        SceneManager.LoadScene("HomeScene");
    }

    public void LoadPuzzlesScene(bool showTips = false)
    {
        BGMusic.Instance.PlayBtnSfx();

        if (showTips)
        {   PlayerPrefs.SetInt("ShowTips", 1);
            PlayerPrefs.SetString("CurrWaysOfWellbeing", m_waysOfWellbeing.ToString());
        }   else
        {   PlayerPrefs.SetInt("ShowTips", 0);
            PlayerPrefs.SetString("CurrWaysOfWellbeing", "");
        }

        SceneManager.LoadScene("PuzzlesScene");
    }
}
