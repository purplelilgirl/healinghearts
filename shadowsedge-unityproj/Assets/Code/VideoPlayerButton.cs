﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class VideoPlayerButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject m_buttonPanel;

    [HideInInspector]
    public bool m_startPlay = false;

    void Start()
    {
        m_buttonPanel.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {   m_buttonPanel.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (m_startPlay)
        {   m_buttonPanel.SetActive(false);
        }
    }
}
