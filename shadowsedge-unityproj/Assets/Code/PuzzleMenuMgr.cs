﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class PuzzleButton
{
    public Button m_button;
    public Sprite m_colored;
    public Text m_text;
    public WayOfWellbeing m_waysOfWellbeing;

    public Button m_icon;
    public GameObject m_title;

    [HideInInspector]
    public PuzzleMenuMgr m_puzzleMenuMgr;

    public void ClickButton()
    {
        bool unlockedTips = PlayerPrefs.GetInt(m_waysOfWellbeing.ToString() + "UnlockedTips", 0) == 1 ? true : false;

        if (unlockedTips)
        {
            BGMusic.Instance.PlayPopupSfx();
            m_puzzleMenuMgr.ShowTips(m_waysOfWellbeing.ToString());
        }   else
        {
            BGMusic.Instance.PlayBtnSfx();
            ClickPuzzle();
        }
    }

    public void ClickPuzzle()
    {
        BGMusic.Instance.PlayBtnSfx();
        m_puzzleMenuMgr.LoadPuzzleScene(m_waysOfWellbeing.ToString() + "Scene");
    }
}

public class PuzzleMenuMgr : MonoBehaviour
{
    public PuzzleButton[] m_puzzleButtons;
    private Dictionary<string, PuzzleButton> m_puzzleDict;

    public GameObject m_popupBG;
    public GameObject m_tipsPopup;
    public Text m_tips;

    public float m_animTime = 0.2f;

    void Start()
    {
        m_puzzleDict = new Dictionary<string, PuzzleButton>();

        for (int i=0; i < m_puzzleButtons.Length; i++)
        {
            m_puzzleDict.Add(m_puzzleButtons[i].m_waysOfWellbeing.ToString(), m_puzzleButtons[i]);

            bool isUnlocked = PlayerPrefs.GetInt(m_puzzleButtons[i].m_waysOfWellbeing.ToString() + "Unlocked", 0)==1?true:false;

            m_puzzleButtons[i].m_button.interactable = isUnlocked;

            if(PlayerPrefs.GetInt(m_puzzleButtons[i].m_waysOfWellbeing.ToString() + "UnlockedTips", 0)==1?true:false)
            {   m_puzzleButtons[i].m_button.GetComponent<Image>().sprite = m_puzzleButtons[i].m_colored;
            }

            m_puzzleButtons[i].m_text.gameObject.SetActive(isUnlocked);
            m_puzzleButtons[i].m_icon.gameObject.SetActive(false);
            m_puzzleButtons[i].m_title.gameObject.SetActive(false);

            m_puzzleButtons[i].m_puzzleMenuMgr = this;
            m_puzzleButtons[i].m_button.onClick.AddListener(m_puzzleButtons[i].ClickButton);
            m_puzzleButtons[i].m_icon.onClick.AddListener(m_puzzleButtons[i].ClickPuzzle);
        }

        if(PlayerPrefs.GetInt("ShowTips", 0) == 1)
        {   ShowTips(PlayerPrefs.GetString("CurrWaysOfWellbeing", "BeAware"), false);
        }   else
        {   m_popupBG.SetActive(false);
            m_tipsPopup.SetActive(false);
        }
    }

    public void ShowTips(string waysOfWellbeing, bool willAnim = true)
    {
        m_popupBG.SetActive(true);

        m_tips.text = PlayerPrefs.GetString(waysOfWellbeing + "Tips", "");

        m_puzzleDict[waysOfWellbeing].m_icon.gameObject.SetActive(true);
        m_puzzleDict[waysOfWellbeing].m_title.SetActive(true);

        if (willAnim)
        {
            m_tipsPopup.transform.localScale = Vector3.zero;
            LeanTween.scale(m_tipsPopup, Vector3.one, m_animTime);
        }

        m_tipsPopup.SetActive(true);
    }

    public void CloseTips()
    {
        BGMusic.Instance.PlayPopupSfx();
        LeanTween.scale(m_tipsPopup, Vector3.zero, m_animTime).setOnComplete(HideTips);
    }

    void HideTips()
    {
        m_popupBG.SetActive(false);
        m_tipsPopup.SetActive(false);
    }
    public void LoadHomeScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        SceneManager.LoadScene("HomeScene");
    }

    public void LoadPuzzleScene(string text)
    {
        SceneManager.LoadScene(text);
    }
}
