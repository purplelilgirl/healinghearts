﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMusic : Singleton<BGMusic>
{
    public AudioSource m_BGM;
    public AudioSource m_btnSfx;
    public AudioSource m_popupSfx;

    public float m_fadeTime = 1;

    public void FadeMusicIn()
    {
        if (m_BGM != null)
        {   m_BGM.Play();
            LeanTween.value(gameObject, FadeMusic, m_BGM.volume, 1, m_fadeTime);
        }   
    }
    public void FadeMusicOut()
    {
        LeanTween.value(gameObject, FadeMusic, 1, 0, m_fadeTime);
    }

    void FadeMusic(float value)
    {
        if (m_BGM != null)
        {
            m_BGM.volume = value;

            if (m_BGM.volume == 0)
            {
                m_BGM.Stop();
            }
        }
    }

    public void PlayBtnSfx()
    {
        if (m_popupSfx != null)
        {   m_btnSfx.Play();
        }
    }

    public void PlayPopupSfx()
    {
        if (m_popupSfx != null)
        {   m_popupSfx.Play();
        }
    }
}
