﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class FiltersMgr : MonoBehaviour
{
    public ColorCorrectionLookup m_colorCorrectionLookup;

    public void ClickFilterBtn(Texture2D filter)
    {
        if (m_colorCorrectionLookup != null)
        {   m_colorCorrectionLookup.Convert(filter, "");
        }
    }
}
