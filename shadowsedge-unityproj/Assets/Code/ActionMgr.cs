﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Action
{
    public Button m_button;

    [HideInInspector]
    public Animator m_animator;

    [HideInInspector]
    public UIMgr m_uiMgr;

    [HideInInspector]
    public ActionMgr m_actionMgr;

    public void PlayAnim()
    {
        //m_animator.PlayInFixedTime(m_button.name, -1, m_uiMgr.m_animTime);

        m_actionMgr.m_nextAction = m_button.name;
        m_uiMgr.CloseNotebook();
    }
}

public class ActionMgr : MonoBehaviour
{
    [HideInInspector]
    public UIMgr m_uiMgr;

    public Animator m_animator;

    public Action[] m_actions;

    [HideInInspector]
    public string m_nextAction = "";

    // Start is called before the first frame update
    public void LoadActions(List<string> unlockActions)
    {
        for(int i=0; i< m_actions.Length; i++)
        {
            m_actions[i].m_uiMgr = m_uiMgr;
            m_actions[i].m_actionMgr = this;
            m_actions[i].m_animator = m_animator;
            m_actions[i].m_button.onClick.AddListener(m_actions[i].PlayAnim);

            string actionName = m_actions[i].m_button.name;

            if (unlockActions.Contains(actionName))
            {   PlayerPrefs.SetInt("Action" + actionName + "Unlocked", 1);
            }

            m_actions[i].m_button.interactable = (PlayerPrefs.GetInt("Action" + actionName + "Unlocked", 0) == 1 ? true : false);
        }
    }

    public void PlayAction(string action, float delay=0)
    {   m_animator.PlayInFixedTime(action, -1, delay);
    }

    public void PlayNextAction()
    {   
        if(m_nextAction != "")
        {
            m_animator.Play(m_nextAction);
            m_nextAction = "";
        }
    }

    public void LoadMixamo()
    {
        Application.OpenURL("https://mixamo.com");
    }
}
