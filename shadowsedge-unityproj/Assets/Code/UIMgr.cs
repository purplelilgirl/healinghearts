﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using NatCorder;
using NatCorder.Clocks;
using NatCorder.Inputs;
using NatMic;


[System.Serializable]
public enum WayOfWellbeing { BeAware, KeepLearning, Connect, BeActive, HelpOthers };

[System.Serializable]
public class VideoPrompt
{
    public string m_prompt;
    
    public string m_tip;
    
    public WayOfWellbeing m_waysOfWellbeing;

    // sticker is unlocked by category, since prompts are incremental, just mention new unlocks
    public string[] m_stickerUnlocks;

    // action is unlocked individually
    public string[] m_actionUnlocks;

    public bool m_puzzleUnlock;
}

public class UIMgr : MonoBehaviour
{
    public bool m_clearCache = false;

    [HideInInspector]
    public int m_currVideoIdx;

    public float m_animTime = 0.5f;

    public GameObject m_popupBG;
    
    public GameObject m_videoPromptPopup;
    public InputField m_videoTitleInput;
    public Text m_videoPromptText;
    public Text m_videoUnlockStartText;

    public GameObject m_videoCompletePopup;
    public InputField m_videoTitleText;
    public Text m_videoTipText;
    public Text m_videoUnlockEndText;

    public GameObject m_feelingsPromptPopup;

    public GameObject m_notebook;

    public GameObject m_UI;

    public GameObject m_recordBtn;
    public GameObject m_pauseBtn;
    public GameObject m_stopBtn;
    public GameObject m_recordIcon;

    public GameObject m_trashBtn;

    private bool m_intro = true;

    public StickerMgr m_stickerMgr;
    public ActionMgr m_actionMgr;
    public FiltersMgr m_filterMgr;

    // shows in background will hide video title a few seconds after recording
    public Text m_videoTitle;

    private List<GameObject> m_stickers;

    [HideInInspector]
    public GameObject m_currSticker;

    public GameObject m_savingVideoTxt;

    public VideoPrompt[] m_videoPrompt;

    private void Start()
    {
        if (m_clearCache)
        {   PlayerPrefs.DeleteAll();
        }

        m_currVideoIdx = PlayerPrefs.GetInt("CurrVideoIdx", 0);
        PlayerPrefs.SetInt("MainSceneVideoIdx", m_currVideoIdx);

        m_popupBG.SetActive(true);

        m_videoCompletePopup.SetActive(false);
        
        m_feelingsPromptPopup.SetActive(false);

        m_notebook.SetActive(false);

        m_pauseBtn.SetActive(false);
        m_stopBtn.SetActive(false);
        m_recordIcon.SetActive(false);

        m_trashBtn.SetActive(false);

        m_filterMgr.gameObject.SetActive(false);

        m_savingVideoTxt.SetActive(false);

        m_stickerMgr.m_uiMgr = this;

        List<string> stickerUnlocks = new List<string>();

        for(int i = 0; i < m_videoPrompt[m_currVideoIdx].m_stickerUnlocks.Length; i++) 
        {   stickerUnlocks.Add(m_videoPrompt[m_currVideoIdx].m_stickerUnlocks[i]);
        }

        m_stickerMgr.LoadIcons(stickerUnlocks);
        
        m_stickerMgr.gameObject.SetActive(true);

        m_actionMgr.m_uiMgr = this;

        List<string> actionUnlocks = new List<string>();

        for (int i = 0; i < m_videoPrompt[m_currVideoIdx].m_actionUnlocks.Length; i++)
        {   actionUnlocks.Add(m_videoPrompt[m_currVideoIdx].m_actionUnlocks[i]);
        }

        m_actionMgr.LoadActions(actionUnlocks);
        m_actionMgr.gameObject.SetActive(false);

        m_stickers = new List<GameObject>();

        LoadVideoPrompt();
        m_videoPromptPopup.SetActive(true);
    }

    void LoadVideoPrompt()
    {
        m_videoPromptText.text = m_videoPrompt[m_currVideoIdx].m_prompt;

        string unlockStartText = "";

        if (m_videoPrompt[m_currVideoIdx].m_stickerUnlocks.Length > 0)
        {
            int stickerCount = 0;

            for (int i = 0; i < m_videoPrompt[m_currVideoIdx].m_stickerUnlocks.Length; i++)
            {
                string category = m_videoPrompt[m_currVideoIdx].m_stickerUnlocks[i];

                List<Sticker> stickers = m_stickerMgr.m_stickersCategories[category];
                stickerCount += stickers.Count;
            }

            unlockStartText = "You will unlock " + stickerCount + " stickers";

            if (m_videoPrompt[m_currVideoIdx].m_actionUnlocks.Length == 0)
            {
                unlockStartText += ".";
            }
            else
            {
                unlockStartText += " and ";
            }
        }

        if (m_videoPrompt[m_currVideoIdx].m_actionUnlocks.Length > 0)
        {
            if (m_videoPrompt[m_currVideoIdx].m_stickerUnlocks.Length == 0)
            {   unlockStartText = "You will unlock ";
            }

            unlockStartText += m_videoPrompt[m_currVideoIdx].m_actionUnlocks.Length + " actions.";
        }

        m_videoUnlockStartText.text = unlockStartText;

        m_videoTipText.text = m_videoPrompt[m_currVideoIdx].m_tip;

        string wellbeing = m_videoPrompt[m_currVideoIdx].m_waysOfWellbeing.ToString();

        if(m_videoPrompt[m_currVideoIdx].m_puzzleUnlock)
        {   m_videoUnlockEndText.text = "You have unlocked \"" + wellbeing + "\" Puzzle.";
        }
        else
        {   m_videoUnlockEndText.text = "";
        }   
    }

    public void LoadHomeScene()
    {
        SceneManager.LoadScene("HomeScene");
    }

    public void ShowNotebook()
    {
        m_popupBG.SetActive(true);

        m_notebook.transform.position = new Vector3(-922.5f, m_notebook.transform.position.y, m_notebook.transform.position.z);
        m_notebook.SetActive(true);

        LeanTween.moveX(m_notebook, 922.5f, m_animTime);

        PauseRecord();
    }

    public void CloseNotebook()
    {
        LeanTween.moveX(m_notebook, -922.5f, m_animTime).setOnComplete(HideNotebook);
    }

    void HideNotebook()
    {
        m_popupBG.SetActive(false);
        m_notebook.SetActive(false);
    }

    public void ShowActions()
    {
        m_actionMgr.gameObject.SetActive(true);
        m_stickerMgr.gameObject.SetActive(false);
        m_filterMgr.gameObject.SetActive(false);
    }
    public void ShowStickers()
    {
        m_actionMgr.gameObject.SetActive(false);
        m_stickerMgr.gameObject.SetActive(true);
        m_filterMgr.gameObject.SetActive(false);
    }

    public void ShowFilters()
    {
        m_filterMgr.gameObject.SetActive(true);
        m_actionMgr.gameObject.SetActive(false);
        m_stickerMgr.gameObject.SetActive(false);
    }

    public void StartVideo()
    {
        BGMusic.Instance.PlayPopupSfx();

        m_videoTitle.text = m_videoTitleInput.text;

        LeanTween.scale(m_videoPromptPopup, Vector3.zero, m_animTime).setOnComplete(HideVideoPrompt);
    }

    void HideVideoPrompt()
    {
        m_popupBG.SetActive(false);
        m_videoPromptPopup.SetActive(false);
    }

    public void StartRecord()
    {
        if (m_intro)
        {
            m_intro = false;
            m_actionMgr.PlayAction("Greet", 0.1f);
            Invoke("HideTitle", 6);

            m_UI.SetActive(false);
            ScreenCapture.CaptureScreenshot(Application.persistentDataPath+"Screenshot" +m_currVideoIdx+".png");
            Invoke("ShowUI", 0.1f);
        }

        m_actionMgr.PlayNextAction();

        m_recordBtn.SetActive(false);
        m_pauseBtn.SetActive(true);
        m_stopBtn.SetActive(true);

        CancelInvoke("BlinkRecordOn");
        CancelInvoke("BlinkRecordOff");
        BlinkRecordOn();
    }
    public void PauseRecord()
    {
        m_recordBtn.SetActive(true);
        m_pauseBtn.SetActive(false);
        m_stopBtn.SetActive(true);

        CancelInvoke("BlinkRecordOn");
        CancelInvoke("BlinkRecordOff");
        m_recordIcon.SetActive(false);
    }
    public void StopRecord()
    {
        BGMusic.Instance.PlayPopupSfx();

        m_recordBtn.SetActive(false);
        m_pauseBtn.SetActive(false);
        m_stopBtn.SetActive(false);

        CancelInvoke("BlinkRecordOn");
        CancelInvoke("BlinkRecordOff");
        m_recordIcon.SetActive(false);

        m_popupBG.SetActive(true);

        m_savingVideoTxt.SetActive(true);
    }

    public void VideoComplete()
    {
        if (m_videoTitleInput.text == "")
        {
            m_videoTitleText.text = "Video " + (m_currVideoIdx + 1).ToString();
        }
        else
        {
            m_videoTitleText.text = m_videoTitleInput.text;
        }

        PlayerPrefs.SetString(m_currVideoIdx + "VideoTitle", m_videoTitleText.text);
        PlayerPrefs.SetString(m_currVideoIdx + "VideoDescription", m_videoPromptText.text);

        if (m_videoPrompt[m_currVideoIdx].m_puzzleUnlock)
        {
            PlayerPrefs.SetInt(m_videoPrompt[m_currVideoIdx].m_waysOfWellbeing.ToString() + "Unlocked", 1);
        }

        // only add tips if completed video
        PlayerPrefs.SetString(m_videoPrompt[m_currVideoIdx].m_waysOfWellbeing.ToString() + "Tips", PlayerPrefs.GetString(m_videoPrompt[m_currVideoIdx].m_waysOfWellbeing.ToString() + "Tips", "") + m_videoPrompt[m_currVideoIdx].m_tip + "\n");
        PlayerPrefs.SetInt("CurrVideoIdx", m_currVideoIdx + 1);

        m_videoCompletePopup.transform.localScale = Vector3.zero;
        m_videoCompletePopup.SetActive(true);
        LeanTween.scale(m_videoCompletePopup, Vector3.one, m_animTime);
    }

    public void WatchVideo()
    {
        BGMusic.Instance.PlayBtnSfx();

        PlayerPrefs.SetInt("FromMainScene", 1);
        PlayerPrefs.SetInt("MainSceneVideoIdx", m_currVideoIdx);

        SceneManager.LoadScene("VideosScene");
    }

    void BlinkRecordOn()
    {
        m_recordIcon.SetActive(true);

        Invoke("BlinkRecordOff", 1);
    }

    void BlinkRecordOff()
    {
        m_recordIcon.SetActive(false);

        Invoke("BlinkRecordOn", 1);
    }

    void HideTitle()
    {   m_videoTitle.gameObject.SetActive(false);

        ShowFeelingsPrompt();
    }

    void ShowUI()
    {
        m_UI.SetActive(true);
    }

    public void AddSticker(Sprite sprite)
    {
        GameObject sticker = new GameObject();
        sticker.transform.parent = m_actionMgr.m_animator.transform;

        sticker.AddComponent<SpriteRenderer>();
        sticker.GetComponent<SpriteRenderer>().sprite = sprite;
        sticker.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 200.0f/255);
        sticker.AddComponent<BoxCollider>();
        sticker.GetComponent<BoxCollider>().isTrigger = true;
        sticker.AddComponent<Rigidbody>();
        sticker.GetComponent<Rigidbody>().isKinematic = true;

        sticker.transform.localPosition = new Vector3(-6, 2, -10);
        sticker.transform.localRotation = Quaternion.Euler(new Vector3(0, -180, 0));
        sticker.transform.localScale = Vector3.zero;
        LeanTween.scale(sticker, Vector3.one, m_animTime).setDelay(m_animTime);

        m_stickers.Add(sticker);

        CloseNotebook();
    }

    public void ShowFeelingsPrompt()
    {
        PauseRecord();

        m_feelingsPromptPopup.transform.localScale = Vector3.zero;
        m_feelingsPromptPopup.SetActive(true);

        LeanTween.scale(m_feelingsPromptPopup, Vector3.one, m_animTime);
    }

    public void SelectFeelings(Sprite sprite)
    {
        m_feelingsPromptPopup.SetActive(false);

        PlayerPrefs.SetString(m_currVideoIdx + "Feelings", sprite.name);

        AddSticker(sprite);
        Invoke("StartRecord", m_animTime);
    }

    public void PlayFeelingAction(string actionName)
    {   
        m_actionMgr.m_nextAction = actionName;
    }

    // deletes the currSticker
    public void ClickTrash()
    {
        m_stickers.Remove(m_currSticker);

        Destroy(m_currSticker);
        m_currSticker = null;

        if(m_stickers.Count == 0)
        {   m_trashBtn.SetActive(false);
        }
    }
}
