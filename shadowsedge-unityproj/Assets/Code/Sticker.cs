﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sticker : MonoBehaviour
{
    [HideInInspector]
    public Sprite m_sprite;

    [HideInInspector]
    public Button m_button;

    [HideInInspector]
    public UIMgr m_uiMgr;

    [HideInInspector]
    public StickerMgr m_stickerMgr;
    public void AddSticker()
    {
        m_uiMgr.AddSticker(m_sprite);
    }
}