﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickerMgr : MonoBehaviour
{
    [HideInInspector]
    public UIMgr m_uiMgr;

    private Dictionary<string, Sprite> m_iconsDict;

    private string[] m_categoryNames;

    [HideInInspector]
    public Dictionary<string, List<Sticker>> m_stickersCategories;
    public Transform m_iconScroll;

    public ColorBlock m_iconColors;
    public void LoadIcons(List<string> unlockStickers)
    {
        m_stickersCategories = new Dictionary<string, List<Sticker>>();
        m_categoryNames = new string[] { "Emotions", "Basics", "Health", "Weather", "Home", "School-Work", "Activities", "People", "Animal-Plants", "Food", "Places", "Transportation" };

        m_iconsDict = new Dictionary<string, Sprite>();

        for (int i = 0; i < m_categoryNames.Length; i++)
        {
            UnityEngine.Object[] icons = Resources.LoadAll("Icons/" + m_categoryNames[i], typeof(Sprite));

            List<Sticker> stickers = new List<Sticker>();
            m_stickersCategories.Add(m_categoryNames[i], stickers);

            bool willUnlock = unlockStickers.Contains(m_categoryNames[i]);

            for (int j = 0; j < icons.Length; j++)
            {
                m_iconsDict.Add(icons[j].name, icons[j] as Sprite);

                GameObject icon = new GameObject();
                icon.transform.parent = m_iconScroll;
                icon.name = icons[j].name;
                icon.AddComponent<CanvasRenderer>();
                icon.AddComponent<Image>();
                icon.GetComponent<Image>().sprite = icons[j] as Sprite;
                icon.AddComponent<Button>();
                icon.AddComponent<Sticker>();

                Sticker sticker = icon.GetComponent<Sticker>();
                sticker.m_stickerMgr = this;
                sticker.m_uiMgr = m_uiMgr;
                sticker.m_button = icon.GetComponent<Button>();
                sticker.m_button.onClick.AddListener(sticker.AddSticker);
                sticker.m_button.colors = m_iconColors;
                sticker.m_sprite = icons[j] as Sprite;

                if(willUnlock)
                {   PlayerPrefs.SetInt("Sticker" + icons[j].name + "Unlocked", 1);
                }

                sticker.m_button.interactable = (PlayerPrefs.GetInt("Sticker" + icons[j].name + "Unlocked", 0)==1 ? true:false);
                
                stickers.Add(sticker);
            }
        }
    }

    public void LoadIcons8()
    {
        Application.OpenURL("https://icons8.com");
    }
}
