﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using NatShare;

public class VideoPlayerPopup : MonoBehaviour
{
    public VideoPlayer m_videoPlayer;
    public Slider m_slider;

    public RawImage m_background;
    public GameObject m_playBtn;
    public GameObject m_pauseBtn;

    public VideoPlayerButton m_videoPlayerButton;

    public AudioSource m_audioSource;
    public Slider m_audioSlider;

    public Text m_videoTitle;
    public Text m_videoDescription;

    public Image m_emoji;

    private void Start()
    {
        m_playBtn.SetActive(true);
        m_pauseBtn.SetActive(false);
    }

    public void SetVideoPlayer(string url)
    {
        m_videoPlayer.url = url;
        m_slider.value = 0;
    }

    public void ClickPlayBtn()
    {
        BGMusic.Instance.PlayBtnSfx();

        m_playBtn.SetActive(false);
        m_pauseBtn.SetActive(true);

        m_videoPlayer.Play();

        m_videoPlayerButton.m_startPlay = true;
        m_videoPlayerButton.m_buttonPanel.SetActive(false);
    }

    public void ClickPauseBtn()
    {
        BGMusic.Instance.PlayBtnSfx();

        m_playBtn.SetActive(true);
        m_pauseBtn.SetActive(false);

        m_videoPlayer.Pause();
    }

    public void OnVolumeChange()
    {
        m_audioSource.volume = m_audioSlider.value;
    }

    void Update()
    {
        if(m_videoPlayer.isPlaying)
        {   m_slider.value = (float) (m_videoPlayer.time / m_videoPlayer.length);

            if (m_slider.value > 0.95f)
            {
                m_playBtn.SetActive(true);
                m_pauseBtn.SetActive(false);

                m_videoPlayerButton.m_buttonPanel.SetActive(true);
            }
        }
    }
}
