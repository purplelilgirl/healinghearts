﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatCorder;
using NatCorder.Clocks;
using NatCorder.Inputs;
using NatMic;

public class RecordingMgr : MonoBehaviour, IAudioProcessor
{
    public UIMgr m_uiMgr;

    [Header("Recording")]
    public int videoWidth = 1920;
    public int videoHeight = 1080;

    private IMediaRecorder m_videoRecorder;
    private RealtimeClock m_clock;
    private CameraInput cameraInput;

    private IAudioDevice m_audioDevice;

    bool m_startedRecording = false;

    public void StartRecording()
    {
        if (!m_startedRecording)
        {
            // Source: https://medium.com/@olokobayusuf/natcorder-tutorial-audio-workflows-1cfce15fb86a

            var sampleRate = 44100;
            var channelCount = 1;

            // Start recording
            m_clock = new RealtimeClock();
            m_videoRecorder = new MP4Recorder(
                videoWidth,
                videoHeight,
                30,
                sampleRate,
                channelCount,
                OnReplay
            );
            // Create recording inputs
            cameraInput = new CameraInput(m_videoRecorder, m_clock, Camera.main);

            // Start the microphone
            m_audioDevice = AudioDevice.GetDevices()[0];

            if (m_audioDevice != null)
            {
                m_audioDevice.StartRecording(sampleRate, channelCount, this);
            }

            m_startedRecording = true;
        }

        else
        {
            ResumeRecording();
        }
         
    }

    // Invoked by NatMic audio device with new audio sample buffer
    public void OnSampleBuffer(float[] sampleBuffer, int sampleRate, int channelCount, long timestamp)
    {
        // Send sample buffers directly to the video recorder for recording
        m_videoRecorder.CommitSamples(sampleBuffer, m_clock.Timestamp);
    }

    // Source: https://medium.com/natsuite/natcorder-and-natmic-a-crash-course-221e58bc3525
    public void PauseRecording()
    {
        // First, we pause the recording clock
        // This will essentially 'freeze' time, reflected in the clock's timestamps
        m_clock.Paused = true;

        // Then we dispose the camera input
        // We do this because we don't want to record anything while recording is paused
        cameraInput.Dispose();
    }

    void ResumeRecording()
    {
        // Now we resume the recording clock
        // The clock's timestamps will now continue from when we paused
        // It won't matter how long the clock was paused, which is what we want!
        m_clock.Paused = false;

        // And of course, we continue recording video frames
        // NOTE: We must use the same recording clock when creating the new recorder
        cameraInput = new CameraInput(m_videoRecorder, m_clock, Camera.main);
    }

    public void StopRecording()
    {

        if (m_audioDevice != null)
        {   // Stop recording
            m_audioDevice.StopRecording();
        }

        cameraInput.Dispose();

        // Stop recording
        m_videoRecorder.Dispose();
    }

    private void OnReplay(string path)
    {
        Debug.Log("Saved recording to: " + path);

        PlayerPrefs.SetString(m_uiMgr.m_currVideoIdx + "VideoPath", path);

        m_uiMgr.VideoComplete();
    }
}
