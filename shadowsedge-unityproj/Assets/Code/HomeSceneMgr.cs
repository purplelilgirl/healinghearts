﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class HomeSceneMgr : MonoBehaviour
{
    public GameObject m_logo;
    public GameObject m_videoPlayerObj;
    public VideoPlayer m_videoPlayer;

    public bool m_playTutorial = true;

    void Start()
    {
        if (m_playTutorial || PlayerPrefs.GetInt("ShowTutorial", 1) == 1)
        {
            m_videoPlayerObj.gameObject.SetActive(true);

            m_videoPlayer.Play();
            m_videoPlayer.isLooping = false;
            m_videoPlayer.loopPointReached += EndReached;

            PlayerPrefs.SetInt("ShowTutorial", 0);
        }
        else
        {
            m_videoPlayerObj.gameObject.SetActive(false);
            StartHomeScene();
        }
    }
    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        m_videoPlayerObj.gameObject.SetActive(false);
        StartHomeScene();
    }

    void StartHomeScene()
    {
        BGMusic.Instance.FadeMusicIn();
        LogoAnim();
    }

    void LogoAnim()
    {
        LeanTween.scale(m_logo, new Vector3(1.01f, 1.01f, 1.01f), 0.43f);
        LeanTween.scale(m_logo, Vector3.one, 0.4f).setDelay(0.43f);
        LeanTween.scale(m_logo, new Vector3(1.01f, 1.01f, 1.01f), 0.43f).setDelay(0.83f);
        LeanTween.scale(m_logo, new Vector3(1.05f, 1.05f, 1.05f), 1.3f).setDelay(1.26f);
        LeanTween.scale(m_logo, Vector3.one, 0.87f).setDelay(2.56f);

        Invoke("LogoAnim", 3.43f);
    }

    public void LoadMainScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        BGMusic.Instance.FadeMusicOut();
        SceneManager.LoadScene("MainScene");
    }

    public void LoadVideosScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        BGMusic.Instance.FadeMusicOut();
        PlayerPrefs.SetInt("FromMainScene", 0);
        SceneManager.LoadScene("VideosScene");
    }
    public void LoadPuzzlesScene()
    {
        BGMusic.Instance.PlayBtnSfx();
        PlayerPrefs.SetInt("ShowTips", 0);
        SceneManager.LoadScene("PuzzlesScene");
    }

}
